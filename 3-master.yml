---
- hosts: masters
  become: yes
  tasks:
    - name: "REPLACE - configure cgroup driver used by kubelet on Master node"
      become: yes
      replace:
        path: /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
        regexp: 'cgroup-driver=systemd'
        replace: 'cgroup-driver="cgroupfs"'
        backup: yes

    - name: "FILE - insert private node IP's into '/etc/default/kubelet' so that it doesn't default to a public IP."
      blockinfile:
        dest: "/etc/default/kubelet"
        marker: "##### {mark} ANSIBLE MANAGED BLOCK #####"
        insertafter: EOF
        content: |
          KUBELET_EXTRA_ARGS=--node-ip={{ ansible_eth1.ipv4.address }}
      when: ansible_eth1.ipv4.address is defined

    - name: "FILE - insert private node IP's into '/etc/default/kubelet' so that it doesn't default to a public IP."
      blockinfile:
        dest: "/etc/default/kubelet"
        marker: "##### {mark} ANSIBLE MANAGED BLOCK #####"
        insertafter: EOF
        create: true
        content: |
          KUBELET_EXTRA_ARGS=--node-ip={{ ansible_default_ipv4.address }}
      when: ansible_eth1.ipv4.address is not defined

    - name: "SYSTEMD - daemon-reload and restart kubelet"
      become: yes
      systemd:
        state: restarted
        daemon_reload: yes
        name: kubelet

      # TODO fix wording
    - debug:
        msg: "The next command assumes Digital Ocean's Private IP was enabled upon creation, meaning it's bound to interface 'eth1'. Else no '--apiserver-advertise-address' argument is given."

    - name: "SHELL - initialize the cluster (using private IP)"
      shell: "kubeadm init --apiserver-advertise-address='{{ ansible_eth1.ipv4.address }}' --pod-network-cidr=192.168.0.0/16 >> cluster_initialized.txt"
      args:
        chdir: $HOME
        # creates: cluster_initialized.txt
      when: ansible_eth1.ipv4.address is defined

    - name: "SHELL - initialize the cluster"
      # 'SystemVerification' errors are ignored, like if you have a very fresh version of 'docker'
      shell: "kubeadm init --ignore-preflight-errors SystemVerification --pod-network-cidr=192.168.0.0/16 >> cluster_initialized.txt"
      args:
        chdir: $HOME
        # creates: cluster_initialized.txt
      when: ansible_eth1.ipv4.address is not defined

    - name: "FILE - create .kube directory"
      file:
        path: $HOME/.kube
        state: directory
        mode: 0750

    - name: "COPY - copy 'admin.conf' to user's kube config"
      copy:
        src: /etc/kubernetes/admin.conf
        dest: $HOME/.kube/config
        remote_src: yes
        owner: "{{ ansible_user }}"

    - name: "SHELL - install pod network RBAC and pods (calico)"
      shell: "kubectl apply -f {{ item }}"
      args:
        chdir: $HOME
        # creates: pod_network_setup.txt
      with_items:
        - "https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/hosted/rbac-kdd.yaml"
        - "https://docs.projectcalico.org/v3.2/getting-started/kubernetes/installation/hosted/kubernetes-datastore/calico-networking/1.7/calico.yaml >> pod_network_setup.txt"
